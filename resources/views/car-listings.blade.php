@extends('layouts.main')
@section('head')
@endsection

@section('content')
<div class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <h1 class="center amber-text text-accent-4">GoSarp</h1>

            <!--            <div class="row center">-->
            <!--                <h5 class="header col s12 light">Serving From Kumasi And Beyond</h5>-->
            <!--            </div>-->
            <div class="row center"> 
                <a href="{{URL::to('/reservation')}}" id="download-button"
                   class="btn-large waves-effect waves-light amber accent-4">Rent Car</a>
                <a href="{{URL::to('/taxi')}}" id="download-button"
                   class="btn-large waves-effect waves-light amber accent-4">Rent Taxi</a>
            </div>
            <br><br>

        </div>
    </div>
    <div class="parallax"><img style="display: block;
    transform: translate3d(-50%, 509px, 0px);" src="/img/home/toyota.jpg" alt="Unsplashed background img 1"></div>
</div>
<div class="row" style="background: #e3e3e3">
    <div class="row">
        <div>
            @foreach($cars as $car)
            <div class="col s12">
                <div class="car-item row" >
                    <br>
                    {{--<div class="col s12 m3 l3">
                        <img src="{{url($car->images[0]->image_path)}}" alt="car" class="img-responsive">
                    </div>--}}
                    <div class="col-sm12 col-md-6 col-lg-1">
                        @foreach($car->images as $ci)
                        <img src="{{url($ci->image_path)}}" height="200" alt="car" class="img-responsive">
                        @endforeach
                    </div>

                    <div class="col s12 m5 l5">
                        <h5 class="header">{{strtoupper($car->model_name)}}</h5>
                        <p><i class="fa fa-tachometer"></i><strong>Type:</strong>{{strtoupper($car->CarType->type)}} </p>
                        <p><i class="fa fa-tachometer"></i><strong>Fuel:</strong>{{strtoupper($car->fuel_type)}} </p>
                        <p><i class="fa fa-cogs"></i><strong>Gear-Box:</strong>{{strtoupper($car->gear())}} </p>
                        <p><i class="fa fa-users"></i><strong>No. of Passengers:</strong> {{strtoupper($car->no_of_passengers)}}</p>
                        <p><i class="fa fa-credit-card"></i><strong>Price :</strong>{{strtoupper($car->cost)}} GHS</p>
                    </div>

                    <div class="box-footer container">
                        <a href="/reservation?car_id={{$car->id}}">
                            <button type="submit" class="btn btn-primary btn-large pull-right">
                                Make Reservation</button>
                        </a>
                    </div>
                    <br>
                </div>
            </div>
            @endforeach
        </div>
    </div>

</div>
<style>
    .header {
        border-bottom: solid grey 0.5px;
    }

    strong{
        margin: 0 3px;
    }

    .car-item{
        background: white;;
        border-radius: 10px;
        box-shadow: 0 1 1 solid rgba(0,0,0,0.2);
        margin: 5px 10px;
    }
</style>
@endsection