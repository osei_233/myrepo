<!DOCTYPE html>
<html>

<head>
    <title>Gosarp</title>
    <link rel="stylesheet" href="{{url('css/public/main.css')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome-4.4.0/css/font-awesome.min.css')}}" />
</head>

<body>
    <header>
        <div class="logo"></div>
        <nav>
            <ul class="nav">
                <li><a href="">Home</a></li>
                <li><a href="{{url('cars')}}">Cars</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="{{url('about')}}">About</a></li>
                <li><a href="{{url('contact')}}">Contact</a></li>
                <li><a href="{{url('auth/login')}}">Log in</a></li>
            </ul>
        </nav>
    </header>
    <section class="wrapper">
        <section class="top">
            <aside>
                <p>Book a ride now</p>
                <a href="{{url('reservation/car')}}">Rent a car</a>
                <a href="{{url('reservation/taxi')}}">Pick a taxi</a>
            </aside>
        </section>
        <section class="bottom">
            <h2>Serving from Kumasi and Beyond</h2>
            <article>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, aliquam odit eius eligendi quas qui, ad, suscipit sint illum, quasi nobis architecto. Architecto culpa quia, accusamus officia ducimus tempora rem.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, aliquam odit eius eligendi quas qui, ad, suscipit sint illum, quasi nobis architecto. Architecto culpa quia, accusamus officia ducimus tempora rem.</article>
            <div class="img">
                <div class="img-container"></div>
                <span class="border"></span>
            </div>
        </section>
        <section class="car-listings">
            <h2 class="title">Take a Look at some of our cars</h2>
            <ul>
                <li><b>RANGE ROVER</b><em>GHC 570</em></li>
                <li><b>LANDCRUISER V8</b><em>GHC 570</em></li>
                <li><b>FORTUNER</b><em>GHC 570</em></li>
                <li><b>SEQUOIA</b><em>GHC 570</em></li>
                <li><b>LAND-CRUISER ST.</b><em>GHC 570</em></li>
                <li><b>PAJERO</b><em>GHC 470</em></li>
                <li><b>HYUNDAI CRETA</b><em>GHC 430</em></li>
                <li><b>H-1</b><em>GHC 550</em></li>
                <li><b>ASX</b><em>GHC 330</em></li>
                <li><b>GRAND i10</b><em>GHC 280</em></li>
                <li><b>VISTA</b><em>GHC 230</em></li>
            </ul>
        </section>
        <section class="car-grid">
           <hr>
            <div class="grid-item">
                <em>Toyota Prado XL</em>
            </div>
            <div class="grid-item">
                <em>Toyota Prado XL</em>
            </div>
            <div class="grid-item">
                <em>Toyota Prado XL</em>
            </div>
        </section>
        <section class="contact">
           <h2 class="title">Contact Us</h2>
            <div class="det">
                <i class="fa fa-map-marker"></i>
                <b>Address</b>
                <em>71 Kings Road, Kumasi</em>
            </div>
            <div class="det">
                <i class="fa fa-envelope-o"></i>
                <b>Email</b>
                <em>gosarprentals@gmail.com</em>
            </div>
            <div class="det">
                <i class="fa fa-mobile"></i>
                <b>Phone</b>
                <em> +233 - 345 3456</em>
            </div>
        </section>
    </section>
    <footer>Copyright © 2016 GoSarp. All rights reserved</footer>
</body>

</html>