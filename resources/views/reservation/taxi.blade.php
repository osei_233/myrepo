@extends('layouts.main')
@section('content')
<div class="container">
    <form action="{{url('reservation')}}" method="post">
       {{ csrf_field() }}
        <div class="card-panel white">
            <span class="num-indicator">1</span>
            <div class="card-content">
                <div class="row">
                    <select class="browser-default" name="car_type">
                        <option value="" disabled selected>Type of Car</option>
                        <option value="Standard">Standard</option>
                        <option value="Executive">Executive</option>
                    </select>
                </div>
                <div class="row">
	            	<select class="browser-default" name="pickup_loc">
	                    <option value="" disabled selected>Pickup Location</option>
	                    @foreach($locations as $location)
	                    <option value="{{$location->id}}">{{$location->name}}</option>
	                    @endforeach
	                </select>                	
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input name="pickup_date" id="pickup_date" type="text" class="date validate">
                        <label for="pickup_date">Pickup Date</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="pickup_time" id="pickup_time" type="text" class="time validate">
                        <label for="pickup_time">Pickup Time</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="card-panel white">
            <span class="num-indicator">2</span>
            <div class="card-content">
				<div class="row">
					<select class="browser-default" name="drop_loc">
					    <option value="" disabled selected>Drop Location</option>
					    @foreach($locations as $location)
					    <option value="{{$location->id}}">{{$location->name}}</option>
					    @endforeach
					</select>	
				</div>

                <input type="hidden" name="type" value="2">
            </div>
        </div>

        <span data-target="confirm-modal" class="btn modal-trigger">continue</span>
        <div id="confirm-modal" class="modal">
            <div class="modal-content">
                <h4>Confirmation</h4>
                <p>Are you sure you want to rent <b class="text-holder car">Toyota</b></p>
                <p>From <b class="text-holder from-loc"></b> to <b class="text-holder to-loc"></b></p>
                <p>Total cost: GHS <b class="cost"></b></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                <input type="submit" class="modal-action btn-flat" value="Done">
            </div>
        </div>
    </form>

</div>
<style type="text/css">
	.num-indicator {
	  position: absolute;
	  left: -45px;
	  width: 30px;
	  height: 30px;
	  border: 1px solid #81d7ff;
	  text-align: center;
	  line-height: 30px;
	  border-radius: 50%;
	}
	.card-panel{
		position: relative;
		margin: 4% 0;
	}
</style>
@endsection