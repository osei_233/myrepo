@extends('layouts.main') @section('content')

<div class="card" style="min-height: 700px">
    <div class="card-content">
        <table>
            <thead>
                <tr>
                    <th data-field="id">Car</th>
                    <th data-field="name">Pick Up Branch</th>
                    <th data-field="price">Pick Up Date</th>
                    <th data-field="price">Pick Up Time</th>
                    <th data-field="price">Drop Branch</th>
                    <th data-field="price">Drop Date</th>
                    <th data-field="price">Drop Time</th>
                    <th data-field="price">Type</th>
                </tr>
            </thead>
            <tbody>
               @foreach($car_reservations as $reservation)
                <tr>
                    <td>{{$reservation->car->model_name}}</td>
                    <td>{{$reservation->pick_up_branch->branch_name}}</td>
                    <td>{{$reservation->pickup_date}}</td>
                    <td>{{$reservation->pickup_time}}</td>
                    <td>{{$reservation->drop_branch_name->branch_name}}</td>
                    <td>{{$reservation->drop_date}}</td>
                    <td>{{$reservation->drop_time}}</td>
                    <td>Car Reservation</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<style>
    section.row{
        width: 80%;
    }
</style>

@endsection