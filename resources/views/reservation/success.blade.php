<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Reservation Successful</title>
    <link rel="stylesheet" href="{{url('css/success.css')}}">
    <link rel="stylesheet" href="{{url('fonts/font-awesome-4.4.0/css/font-awesome.min.css')}}">
</head>

<body>
       <header><div class="logo"></div></header>
        <div class="banner">
               <i class="fa fa-check"></i>
			<p>
				<b>Thank you for your booking!!</b>
				<span>An email has been sent</span>
				<span style="margin-bottom:5%">We will get back to you shortly</span>
			</p>

			<a href="{{url('user/dashboard')}}">Dashboard</a>
			<a href="{{url('/')}}">Home</a>
        </div>
       	<footer>
       		&copy;2016 | Made by OasisWebsoft
       	</footer>
</body>
</html>