@extends('layouts.main')
@section('content')
<div class="container">
    <form action="{{url('reservation')}}" method="post">
       {{ csrf_field() }}
        <div class="card-panel white">
            <div class="card-content">
                <div class="row">
                    <select class="browser-default" name="car_id">
                        <option value="" disabled selected>Choose a car</option>
                        @foreach($cars as $car)
                        <option data-type="{{$car->type}}" value="{{$car->id}}" @if($car->id==$car_id)selected="selected" @endif>{{$car->model_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="pickup_branch">
                            <option value="" disabled selected>Pickup Branch</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input name="pickup_date" id="pickup_date" type="text" class="date validate">
                        <label for="pickup_date">Pickup Date</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="pickup_time" id="pickup_time" type="text" class="time validate">
                        <label for="pickup_time">Pickup Time</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="card-panel white">
            <div class="card-content">
                <div class="row">
                    <div class="input-field col s12">
                        <select name="drop_branch">
                            <option value="" disabled selected>Drop Branch</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="drop_date" id="drop_date" type="text" class="date validate">
                        <label for="drop_date">Drop Date</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input name="drop_time" id="drop_time" type="text" class="time validate">
                        <label for="drop_time">Drop Time</label>
                    </div>
                </div>
            </div>
        </div>

        <span data-target="confirm-modal" class="btn modal-trigger">continue</span>
        <div id="confirm-modal" class="modal">
            <div class="modal-content">
                <h4>Confirm Booking</h4>
                <p>Price per day is GHC  <b class="cost"></b></p>
                <p>Please confirm by clicking on proceed.</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                <input type="submit" class="modal-action btn-flat" value="Proceed">
            </div>
        </div>
    </form>

</div>
@endsection