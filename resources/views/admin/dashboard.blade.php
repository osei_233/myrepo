<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin Dashboard</title>
    <link type="text/css" rel="stylesheet" href="{{url('css/materialize.min.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{url('css/dashboard.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{url('fonts/font-awesome-4.4.0/css/font-awesome.min.css')}}"  media="screen,projection"/>
    <meta name="csrf-token" content="{{csrf_token()}}">

</head>
<body>

<section class="wrapper">
      <ul class="side-nav fixed">
        <div class="logo-wrapper">
          <div class="logo"></div>
        </div>
         <li class="user-info">
           <div class="img"></div>
           <span class="info">
             <strong>{{$username}}</strong>
           </span>
         </li>
         <li class="label">MENU</li>
          <ul class="collapsible collapsible-accordion">
              <li class="bold"><i class="fa fa-users"></i><a class="collapsible-header  waves-effect waves-red">Users</a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a class="view-btn" href="view/users/users">Users</a></li>
                        <li><a class="view-btn" href="view/users/admin">Admins</a></li>
                      </ul>
                    </div>
                  </li>           
                   
                   <li class="bold"><i class="fa fa-car"></i><a class="collapsible-header  waves-effect waves-red">Cars</a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a class="view-btn" href="view/cars/all">All</a></li>
                        <li><a class="view-btn" href="view/cars/types">Types</a></li>
                      </ul>
                    </div>
                  </li>
                                   
                   <li class="bold"><i class="fa fa-industry"></i><a class="collapsible-header  waves-effect waves-red">Branches</a>
                    <div class="collapsible-body">
                      <ul>
                          <li><a class="view-btn" href="view/branches/all">All</a></li>
                      </ul>
                    </div>
                  </li>

                   <li class="bold"><i class="fa fa-question-circle"></i><a class="collapsible-header  waves-effect waves-red">Enquiries</a>
                    <div class="collapsible-body">
                      <ul>
                          <li><a class="view-btn" href="view/enquiries/all">All enquries</a></li>
                         <li><a class="view-btn" href="view/feedbacks/all">Feedbacks</a></li>
                      </ul>
                    </div>
                  </li>

                   <li class="bold"><i class="fa fa-table"></i><a class="collapsible-header  waves-effect waves-red">Reservations</a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a class="view-btn" href="view/reservations/cars">All</a></li>
                      </ul>
                    </div>
                  </li>            
          </ul>
      </ul>
      <section class="content">
            <p class="hd">Super Admin Dashboard <a>Summary Page<a></p>
            <div class="stats">
                <div class="stat-panel" style="background: #1976d2"><h2>{{$stat['cars']}}</h2> cars</div>
                <div class="stat-panel" style="background: #4db6ac"><h2>{{$stat['users']}}</h2> users</div>
                <div class="stat-panel" style="background: #7986cb"><h2>{{$stat['admins']}}</h2> admins</div>
                <div class="stat-panel" style="background: #d21919"><h2>{{$stat['branches']}}</h2> branches</div>
                <div class="stat-panel" style="background: #1976d2"><h2>{{$stat['enquiries']}}</h2> enquiries</div>
                <div class="stat-panel" style="background: #4db6ac"><h2>{{$stat['reservations']}}</h2> reservations</div>
            </div>  
      </section>
    </section>
    <script src="{{url('js/jquery.js')}}"></script>
    <script src="{{url('js/materialize.min.js')}}"></script>
    <script src="{{url('js/velocity.js')}}"></script>
    <script src="{{url('js/velocity.ui.js')}}"></script>
    <script src="{{url('js/dashboard.js')}}"></script>
    <script src="{{url('js/dash.js')}}"></script>
    <script src="{{url('js/admin.js')}}"></script>
</body>
</html>