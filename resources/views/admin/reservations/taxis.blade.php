@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Taxi Reservations</p>

</div>
<div class="row" style="background: #ffffff; padding: 10px">
    <div class="col s10">
        <table class="striped">
            <thead>
                <tr>
                    <th data-field="code">Code</th>
                    <th data-field="user">User</th>
                    <th data-field="car">Car Type</th>
                    <th data-field="pickup-date">Pickup date</th>
                    <th data-field="pickup-time">Pickup Time</th>
                    <th data-field="pickup-location">Pickup location</th>
                    <th data-field="drop-date">Drop date</th>
                    <th data-field="drop-time">Drop Time</th>
                    <th data-field="drop-location">Drop location</th>
                </tr>
            </thead>

            <tbody>
               @foreach($models as $reservation)
                <tr>
                    <td>GSTR{{$reservation->id}}</td>
                    <td>
                        <p><strong>Name:</strong>{{$reservation->user->firstname or '-'}} {{$reservation->user->lastname or '-'}}</p>
                        <p><strong>Email:</strong>{{$reservation->user->email or ''}}</p>
                        <p><strong>Cntact:</strong>{{$reservation->user->phone or ''}}</p></td>
                    <td>{{$reservation->car_type}}</td>
                    <td>{{$reservation->pickup_date}}</td>
                    <td>{{$reservation->pickup_time}}</td>
                    <td>{{$reservation->pickup_location->name}}</td>
                    <td>{{$reservation->drop_date}}</td>
                    <td>{{$reservation->drop_time}}</td>
                    <td>{{$reservation->drop_location->name}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col s2">
<!--        <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">-->
<!--            <i class="large fa fa-plus"></i>-->
<!--        </a>-->
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Taxi</h4>
        <div class="row">
            <div class="input-field col s6">
                <select>
                    <option value="" disabled selected>From</option>
                    <option value="1">Standard</option>
                    <option value="2">Executive</option>
                </select>
            </div>
            <div class="input-field col s6">
                <select>
                    <option value="" disabled selected>Destination</option>
                    <option value="1">Standard</option>
                    <option value="2">Executive</option>
                </select>
            </div>
            <div class="input-field col s12">
                <input id="price" type="number" class="validate">
                <label for="price">Price</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="add.taxi" href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
    <div class="modal-content">
        <h4>Delete Permanently</h4>
        <p>Are you sure you want to delete <b></b></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="del modal-action modal-close waves-effect waves-red btn-flat del">Delete</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
    </div>
</div>

@endsection