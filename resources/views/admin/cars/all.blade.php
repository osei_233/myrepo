@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Cars</p>

</div>
<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
            @if(count($models['cars']) == 0)
            <p class='error-message'>Sorry there aren't any cars!</p>
            @endif
            @foreach($models['cars'] as $car)
            <li data-id="{{$car->id}}">
                <div class="collapsible-header name">{{$car->model_name}}</div>
                <div class="collapsible-body item-props">
                    <div>
                        <strong>Name: </strong><span class="car_name">{{$car->model_name}}</span>
                    </div>
                    <div>
                        <strong>Fuel: </strong><span class="fuel_usage">{{$car->fuel_type}}</span>
                    </div> 
                    <div>
                        <strong>Passengers: </strong><span class="passengers">{{$car->no_of_passengers}}</span>
                    </div>
                    <div>
                        <strong>Number Plate: </strong><span class="plate_number">{{$car->plate_number}}</span>
                    </div>
                     <div>
                        <strong>Gear-Box: </strong><span class="plate_number">{{$car->gear}}</span>
                    </div>
                    <div>
                        <strong>Price: </strong><span class="plate_number">GHC {{$car->pricing_per_day}}</span>
                    </div>
                    <hr>
                    <div>
                        @foreach($car->images as $ci)
                        <img src="{{url($ci->image_path)}}" height="100" alt="car" class="img-responsive">
                        @endforeach
                    </div>

                    <a href="#!" class="buttonset">
                        <i data-action="updateCar" data-target="edit-modal" class="modal-trigger edit tiny fa fa-pencil"></i>
                        <i data-action="delCar" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col s2">
        <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">
            <i class="large fa fa-plus"></i>
        </a>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Car</h4>
        <div class="row">
            <div class="input-field col s12">
                <input name="brand" id="brand" type="text" class="validate">
                <label for="brand">Brand</label>
            </div>
            <div class="input-field col s12">
                <input name="model_name" id="name" type="text" class="validate">
                <label for="name">Model</label>
            </div>
            <div class="input-field col s6">
                <select name="car_type_id">
                    <option value="" disabled selected>Car Type</option>
                    @foreach($models['types'] as $type)
                    <option value="{{$type->id}}">{{$type->type}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-field col s6">
                <select name="gear">
                    <option value="" disabled selected>Gear Box</option>
                    <option value="Manual">Manual</option>
                    <option value="Automatic">Automatic</option>
                </select>
            </div>
            <div class="input-field col s6">
                <input name="no_of_passengers" id="passengers" type="text" class="validate">
                <label for="passengers">Maximum number of passengers</label>
            </div>
            <div class="input-field col s12">
                <select name="fuel_type">
                    <option value="" disabled selected>Fuel Type</option>
                    <option value="Diesel">Diesel</option>
                    <option value="Petrol">Petrol</option>
                </select>
            </div>
            <div class="input-field col s12">
                <input name="plate_number" id="plate_number" type="text" class="validate">
                <label for="plate_number">Plate Number</label>
            </div>
            <div class="input-field col s12">
                <select name="current_location">
                    <option value="" disabled selected>Current Location</option>
                    @foreach($models['branches'] as $branch)
                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-field col s6">
                <input name="pricing_per_day" id="pricing_per_day" type="text" class="validate">
                <label for="pricing_per_day">Price per day</label>
            </div>
            <p>
                <input name="availability" type="checkbox" id="avail" />
                <label for="avail">Available</label>
            </p>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" name="image[]">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" name="image[]">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>
        <div class="file-field input-field">
            <div class="btn">
                <span>Image</span>
                <input type="file" name="image[]">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
        </div>
        </div>
    <div class="modal-footer">
        <a data-action="addCar" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
    <div class="modal-content">
        <h4>Delete Permanently</h4>
        <p>Are you sure you want to delete <b class="text-holder"></b></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="del modal-action waves-effect waves-red btn-flat del">Delete</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
    </div>
</div>

@endsection