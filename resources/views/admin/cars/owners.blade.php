@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Car Owners</p>

</div>
<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
           @foreach($models as $owner)
            <li data-id="{{$owner->id}}">
                <div class="collapsible-header name">{{$owner->name}}</div>
                <div class="collapsible-body item-props">
                   
                    <span class="name">{{$owner->name}}</span>
                    <span class="email">{{$owner->email}}</span>
                    <span class="phone">{{$owner->phone}}</span>

                    <a href="#!" class="buttonset">
                        <i data-action="update.carOwner" data-target="edit-modal" class="modal-trigger edit tiny fa fa-pencil"></i>
                        <i data-action="del.carOwner" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>
                    </a>
                </div>
            </li>
          @endforeach
        </ul>
    </div>
    <div class="col s2">
        <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">
            <i class="large fa fa-plus"></i>
        </a>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Car Owner</h4>
        <div class="row">
            <div class="input-field col s12">
                <input name="name" id="name" type="text" class="validate">
                <label for="name">Name</label>
            </div>            
            <div class="input-field col s12">
                <input name="email" id="email" type="text" class="validate">
                <label for="email">Email</label>
            </div>
            <div class="input-field col s12">
                <input name="phone" id="phone" type="text" class="validate">
                <label for="phone">Phone</label>
            </div>
           
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="add.carOwner" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
    <div class="modal-content">
        <h4>Delete Permanently</h4>
        <p>Are you sure you want to delete <b class="text-holder"></b></p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="del modal-action waves-effect waves-red btn-flat del">Delete</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
    </div>
</div>

@endsection