@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Car Features</p>

</div>
<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
           @foreach($models as $feature)
            <li data-id="{{$feature->id}}">
                <div class="collapsible-header name">{{$feature->feature}}</div>
                <div class="collapsible-body item-props">
                    <span class="feature">{{$feature->feature}}</span>

                    <a href="#!" class="buttonset">
                        <i data-action="update.carFeature" data-target="edit-modal" class="modal-trigger edit tiny fa fa-pencil"></i>
                        <i data-action="del.carFeature" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>                    
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col s2">
            <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">
        <i class="large fa fa-plus"></i>
    </a>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Car Feature</h4>
        <div class="row">
            <div class="input-field col s12">
                <input name="feature" id="feature" type="text" class="validate">
                <label for="feature">Feature</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="add.carFeature" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
  <div class="modal-content">
    <h4>Delete Permanently</h4>
    <p>Are you sure you want to delete <b class="text-holder"></b></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="del modal-action modal-close waves-effect waves-red btn-flat del">Delete</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Cancel</a>
  </div>
</div>

@endsection