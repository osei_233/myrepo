@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Users</p>

</div>
<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
           @foreach($models[0] as $key => $user)
            <li data-id="{{$user->id}}">
                <div class="collapsible-header name">{{$user->firstname.' '.$user->lastname}} </div>
                <div class="collapsible-body item-props">
                       
                    <p class="firstname">{{$user->firstname}}</p>
                    <p class="lastname">{{$user->lastname}}</p>
                    <p class="phone" >{{$user->phone}}</p>
                    <p class="email" >{{$user->email}}</p>
                        
                    <a href="#!" class="buttonset">
                        <i data-action="updateUser" data-target="edit-modal" class="modal-trigger edit tiny fa fa-pencil"></i>
                        <i data-action="delUser" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>                    
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col s2">
            <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">
        <i class="large fa fa-plus"></i>
    </a>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add User</h4>
        <div class="row">
            <div class="input-field col s6">
                <input name="firstname" id="firstname" type="text" class="validate">
                <label for="firstname">Firstname</label>
            </div>

            <div class="input-field col s6">
                <input name="lastname" id="lastname" type="text" class="validate">
                <label for="lastname">Lastname</label>
            </div>

            <div class="input-field col s12">
                <input name="phone" id="phone" type="number" class="validate">
                <label for="phone">Phone number</label>
            </div>

            <div class="input-field col s6">
                <input name="email" id="email" type="email" class="validate">
                <label for="email">Email</label>
            </div>
            <div class="input-field col s6">
                <select name="sex">
                    <option value="" disabled selected>Sex</option>
                    <option value="m">M</option>
                    <option value="f">F</option>
                </select>
            </div>
           <div class="input-field col s6">
                <select name="branch">
                    <option value="" disabled selected>Branch</option>
                    @foreach($models[1] as $branch)
                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-field col s12">
                <input name="password" id="password" type="password" class="validate">
                <label for="password">Password</label>
            </div>
            <div class="input-field col s12">
                <input name="re_password" id="re_password" type="password" class="validate">
                <label for="re_password">Retype Password</label>
            </div>
            <input type="hidden" name="type" value="1">
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="addUser" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
  <div class="modal-content">
    <h4>Delete Permanently</h4>
    <p>Are you sure you want to delete <b class="text-holder"></b></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="del modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
  </div>
</div>

<style>
    .collapsible p{
        margin: 2px;
        padding: 0px;
    }
</style>

@endsection