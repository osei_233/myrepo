@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Branches</p>

</div>
<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
           @foreach($models as $key => $branch)
            <li data-id="{{$branch->id}}">
                <div class="collapsible-header name">{{$branch->branch_name}} </div>
                <div class="collapsible-body item-props">
                       
                    <p class="branch_name">{{$branch->branch_name}}</p>
                    <p class="location">{{$branch->location}}</p>
                    <p class="telephone" >{{$branch->telephone}}</p>
                        
                    <a href="#!" class="buttonset">
                        <i data-action="updateBranch" data-target="edit-modal" class="modal-trigger edit tiny fa fa-pencil"></i>
                        <i data-action="deleteBranch" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>                    
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="col s2">
            <a class="btn-floating waves-effect btn-large red modal-trigger" data-target="edit-modal">
        <i class="large fa fa-plus"></i>
    </a>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Branch</h4>
        <div class="row">
            <div class="input-field col s6">
                <input name="branch_name" id="branch_name" type="text" class="validate">
                <label for="branch_name">Branch name</label>
            </div>

            <div class="input-field col s6">
                <input name="location" id="location" type="text" class="validate">
                <label for="location">Branch Location</label>
            </div>

            <div class="input-field col s6">
                <input name="telephone" id="telephone" type="text" class="validate">
                <label for="telephone">Telephone</label>
            </div>
            <div class="input-field col s6">
                <input name="email" id="email" type="email" class="validate">
                <label for="email">Email</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="addBranch" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
  <div class="modal-content">
    <h4>Delete Permanently</h4>
    <p>Are you sure you want to delete <b class="text-holder"></b></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="del modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
  </div>
</div>

<style>
    .collapsible p{
        margin: 2px;
        padding: 0px;
    }
</style>

@endsection