@extends('layouts.dashboard') @section('content')
<div class="head">
    <p>All Enquiries</p>
</div>

<div class="row">
    <div class="col s10">
        <ul class="collapsible" data-collapsible="accordion">
           @foreach($models as $key => $enquiry)
            <li data-id="{{$enquiry->id}}">
                <div class="collapsible-header name">{{$enquiry->username}} </div>
                <div class="collapsible-body item-props">
                       
                    <p class="firstname">{{$enquiry->enquiry}}</p>
                        
                    <a href="#!" class="buttonset">
                        <i data-action="replyEnquiry" data-target="edit-modal" class="modal-trigger edit tiny fa fa-mail-forward"></i>
                        <i data-action="delEnquiry" data-target="delete-modal" class="modal-trigger edit tiny fa fa-trash"></i>                    
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>


<!-- Modal Structure -->
<div id="edit-modal" class="modal">
    <div class="modal-content">
        <h4>Add Feedback</h4>
        <div class="row">
            <div class="input-field col s12">
                <textarea id="feedback" class="materialize-textarea"></textarea>
                <label for="feedback">Please fill in your feedback</label>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a data-action="addUser" href="#!" class="done-btn modal-action waves-effect waves-green btn-flat">Done</a>
    </div>
</div>

<!--Delete Modal-->
<div id="delete-modal" class="modal">
  <div class="modal-content">
    <h4>Delete Permanently</h4>
    <p>Are you sure you want to delete <b class="text-holder"></b></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="del modal-action modal-close waves-effect waves-red btn-flat">Delete</a>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
  </div>
</div>

<style>
    .collapsible p{
        margin: 2px;
        padding: 0px;
    }
</style>

@endsection