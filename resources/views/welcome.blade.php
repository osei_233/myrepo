@extends('layouts.main')

@section('content')
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>

            <h1 class="header center amber-text text-accent-4">UrbanRentals</h1>

            <div class="row center">
                <h5 class="header col s12 light">Serving From Accra And Beyond</h5>
            </div>
            <div class="row center">
                <a href="{{URL::to('/reservation')}}" id="download-button"
                   class="btn-large waves-effect waves-light amber accent-4">Reserve a Car</a>
            </div>
            <br><br>

        </div>
    </div>
    <div class="parallax"><img src="/img/home/toyota.jpg" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="fa fa-flash"></i></h2>
                    <h5 class="center">Speeds of Service</h5>

<!--                    <p class="light">We did most of the heavy lifting for you to provide a default stylings that-->
<!--                        incorporate our custom components. Additionally, we refined animations and transitions to-->
<!--                        provide a smoother experience for developers.</p>-->
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="fa fa-users"></i></h2>
                    <h5 class="center">Customer Experience Focused</h5>

<!--                    <p class="light">By utilizing elements and principles of Material Design, we were able to create a-->
<!--                        framework that incorporates components and animations that provide more feedback to users.-->
<!--                        Additionally, a single underlying responsive system across all platforms allow for a more-->
<!--                        unified user experience.</p>-->
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="fa fa-clock-o"></i></h2>
                    <h5 class="center">Easy to work with</h5>

<!--                    <p class="light">We have provided detailed documentation as well as specific code examples to help-->
<!--                        new users get started. We are also always open to feedback and can answer any questions a user-->
<!--                        may have about Materialize.</p>-->
                </div>
            </div>
        </div>

    </div>
</div>

<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">Fast and Furious</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="/img/home/landcruiser-v8.jpg" alt="Unsplashed background img 3"></div>
</div>
<style type="text/css">
    .row{
        margin-bottom: 0;
    }
</style>
@endsection
