<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gosarp | {{$title}}</title>
    <link type="text/css" rel="stylesheet" href="{{url('css/materialize.min.css')}}"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.datetimepicker.min.css')}}"/>
    <link rel="stylesheet" href="{{url('css/public/reservation.css')}}">
</head>
<body>
   <header>
       <a href="{{url('/')}}">
           <div class="logo"></div>
       </a>
    </header>
    <section class="content-wrapper">
        @yield('content')
    </section>
    <script src="{{url('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{url('js/materialize.min.js')}}"></script>
    <script src="{{url('js/jquery.datetimepicker.full.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/reservation.js')}}"></script>

    @yield('script')
</body>
</html>