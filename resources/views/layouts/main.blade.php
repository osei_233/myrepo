<?php
use Illuminate\Support\Facades\Auth;
?>

<!DOCTYPE html>
<html>

<head>
    <title>UrbanRentals| {{$title or ''}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" href="{{url('fonts/font-awesome-4.4.0/css/font-awesome.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{url('css/materialize.min.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{url('css/style.css')}}"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="{{url('css/jquery.datetimepicker.min.css')}}"/>
    @yield('head')
</head>

<body>

<nav class="blue white-text" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="/" class="brand-logo">
            <img height="45" src="{{url('/img/home/gosarp-white.png')}}"/>
        </a>
        <ul class="right hide-on-med-and-down">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/cars/">Car Listings</a></li>
            <li><a href="/reservation">Rent a Car</a></li>
            

            @if(Auth::user())
            <li><a id="log-in-link" class="nav-link" href="/dashboard">Dashboard</a></li>
            <li><a id="log-in-link" class="nav-link" href="/auth/logout">Logout</a></li>
            @else
            <li><a id="sign-up-link" class="nav-link" href="/auth/signup">Sign Up</a></li>
            <li><a id="log-in-link" class="nav-link" href="/auth/login">Log In</a></li>
            @endif

        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li class="active"><a href="/">Home</a></li>
            <li><a href="/cars/">Car Listings</a></li>
            <li><a href="/reservation">Rent a Car</a></li>

            @if(Auth::user())
            <li><a id="log-in-link" class="nav-link" href="/admin/dashboard">Dashboard</a></li>
            @else
            <li><a id="sign-up-link" class="nav-link" href="/auth/signup">Sign Up</a></li>
            <li><a id="log-in-link" class="nav-link" href="/auth/login">Log In</a></li>
            @endif

        </ul>
        <!--        <ul class="nav navbar-nav">-->
        <!--        </ul>-->

        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons white-text">menu</i></a>
    </div>
</nav>

<section class="row">
    @yield('content')
</section>

<footer class="page-footer teal">
    <div class="container row" style="text-align: center">
        <div class="row">
            <h5 class="white-text">Contact Us</h5>
        </div>
        <div class="row" style="color: white">
            <div class="col s12">
                <i class="fa fa-map-marker"></i>
                <b>Address</b>
                <em>of the ips-legon Road, Accra</em>
            </div>
            <div class="col s12">
                <i class="fa fa-envelope-o"></i>
                <b>Email</b>
                <em>urbanrentalsgh@gmail.com</em>
            </div>
            <div class="col s12">
                <i class="fa fa-mobile"></i>
                <b>Phone</b>
                <em> +233 - 209085784</em>
            </div>
        </div>
    </div>
</footer>

<style>
    .home-page-car-list img{
        height: 100px;
    }

    .car-area{
        background: #3F7FBF;
        padding: 10px;
    }
    nav {
        background-color: #26428E;
    }
</style>

<!--  Scripts-->
<script src="{{url('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{url('js/materialize.min.js')}}"></script>
<script src="{{url('js/jquery.datetimepicker.full.min.js')}}"></script>
<script src="{{url('js/init.js')}}"></script>
<script type="text/javascript" src="{{url('js/reservation.js')}}"></script>
</body>

</html>