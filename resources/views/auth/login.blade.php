@extends('layouts.main')
@section('content')

<div class="container">
    <form action="{{url('auth/login')}}" method="post">
        <p class="center-align">Login</p>
        <div class="card-panel white">
            <div class="card-content">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="input-field col s12">
                        <input name="email" id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12">
                        <input name="password" id="password" type="password" class="validate">
                        <label for="password">Password</label>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" class="submit-btn btn center-align blue" value="login">
    </form>
    <p class="center-align sign-up-caption">Don't have an account? <a href="{{url('auth/signup')}}">Sign Up</a></p>

</div>
<style>
    section.content-wrapper{
        width: 400px;
    }
    .submit-btn{
        margin: 0 auto;
        display: block;
    }
    p.sign-up-caption{
        margin-top: 20%;
    }
    form{
        width: 50%;
        margin: 0 auto;
    }
    
</style>
@endsection