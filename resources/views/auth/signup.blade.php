@extends('layouts.main')
@section('content')
<div class="container">
    <form action="{{url('auth/register')}}" method="post">
        <p class="center-align">Sign Up</p>
        <div class="card-panel white">
            <div class="card-content">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="input-field col s6">
                        <input name="firstname" id="firstname" type="text" class="validate">
                        <label for="firstname">Firstname</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="lastname" id="lastname" type="text" class="validate">
                        <label for="lastname">Lastname</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="telephone" id="phone" type="number" class="validate">
                        <label for="phone">Phone number</label>
                    </div>

                    <div class="input-field col s6">
                        <input name="email" id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12">
                        <select name="sex">
                            <option value="" disabled selected>Sex</option>
                            <option value="m">M</option>
                            <option value="f">F</option>
                        </select>
                    </div>
                    <div class="input-field col s12">
                        <select name="branch">
                            <option value="" disabled selected>Branch</option>
                            @foreach($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-field col s6">
                        <input name="password" id="password" type="password" class="validate">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col s6">
                        <input name="re_password" id="re_password" type="password" class="validate">
                        <label for="re_password">Retype Password</label>
                    </div>
                </div>
            </div>
        </div>
        <input type="submit" class="submit-btn btn center-align" value="sign up">
    </form>

</div>
@endsection