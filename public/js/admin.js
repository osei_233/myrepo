//Dashboard dependent global variables 
globals.item = $('li').find('.name');
globals.editModal = $('#edit-modal');

//User Ajax Actions
Actions.addUser = function (e) {
  data = serializeFormData();

  if (data.password == data.re_password) {
    delete data.re_password;
    Dashboard.actionAjax('action/user');
  } else {
    Materialize.toast('password doesnt match', 2000);
  }
}

Actions.updateUser = function (e) {
  Dashboard.bindToMany(false, e, 'input');
  Dashboard.initUpdate(e, 'action/user');
}

Actions.delUser = function (e) {
  Dashboard.initDelete(e, 'action/user');
}

Actions.addAdmin = function (e) {
  data = serializeFormData();
  
  if (data.password == data.re_password) {
    delete data.re_password;
    Dashboard.actionAjax('action/admin');
  } else {
    Materialize.toast('password doesnt match', 2000);
  }
}

Actions.delAdmin = function(e){
    Dashboard.initDelete(e, 'action/admin');
}


//Car Ajax Actions
Actions.addCar = function (e) {
    Dashboard.actionAjax('action/car', false, false, false, true);
}

Actions.delCar = function (e) {
    Dashboard.initDelete(e, 'action/car');
}

Actions.updateCar = function (e) {
    Dashboard.bindToMany(false, e, 'input');
    Dashboard.initUpdate(e, 'action/car');
}

Actions.addCarType = function (e) {
    Dashboard.actionAjax('action/car-type');
}

Actions.delCarType = function (e) {
    Dashboard.initDelete(e, 'action/car-type');
}

Actions.updateCarType = function (e) {
    Dashboard.bindToMany(false, e, 'input');
    Dashboard.initUpdate(e, 'action/car-type');
}

//Branches Ajax Actions
Actions.addBranch = function (e) {
    Dashboard.actionAjax('action/branch', false, false, false, false);

}

Actions.deleteBranch = function (e) {
    Dashboard.initDelete(e, 'action/branch');
}


//Enquiry
Actions.replyEnquiry = function(){

}

Actions.delEnquiry = function(){
    
}
