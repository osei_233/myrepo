$(document).ready(function () {
    var carTypeSelect = $('#car-type-select');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="_token"]').val()
        }
    });

    $('.modal-trigger').leanModal({
        ready: function () {
            $('.text-holder.car').text($('[name=car] option:selected').text());
            $('.text-holder.from-loc').text($('[name=pickup_loc] option:selected').text());
            $('.text-holder.to-loc').text($('[name=drop_loc] option:selected').text());

            $.ajax({
                url: 'reservation/pricing',
                type: 'post',
                data: {
                    car_id: $('[name=car_id] option:selected').val(),
                },
                success: function (response) {
                    $('.cost').text(response.price);
                },
                error: function () {
                    console.log('oops!');
                }
            });
        }
    });

    carTypeSelect.change(function () {
        var cars = $('[name=car_id]').children('option');
        cars.hide();

        $('[name=car_id]').find('[data-type=' + $(this).find(':selected').val() + ']').show();
    });


    $('.date').datetimepicker({
        timepicker: false,
        format: 'd/m/Y',
        minDate: '0'
    });

 
    var currentTime = new Date().getTime();
    
    $('.time').datetimepicker({
        datepicker: false,
        format: 'H:i',
        minTime: new Date(currentTime + 900000),
        step: 15
    });
});