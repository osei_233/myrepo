<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('enquiries', function (Blueprint $table) {
          $table->increments('id');
          $table->string('username');
          $table->string('email');
          $table->string('enquiry');
          $table->string('telephone_no');
          $table->timestamps();  //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('enquiries');  //
    }
}
