<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cars', function (Blueprint $table) {
          $table->increments('id');
          $table->string('model_name', 20);
          $table->integer('car_type_id');
          $table->string('brand');
          $table->integer('color');
          $table->string('gear');
          $table->boolean('availability');
          $table->string('no_of_passengers');
          $table->integer('pricing_per_day');
          $table->string('plate_number',15);
          $table->string('fuel_type',20);
          $table->string('current_location',10);
          $table->timestamps();
    
        });//
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cars');
    }
}
