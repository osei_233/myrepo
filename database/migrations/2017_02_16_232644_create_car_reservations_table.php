<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_reservations', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('car_id');
          $table->string('pickup_branch',15);
          $table->string('drop_branch',15);
          $table->string('pickup_time');
          $table->string('drop_time');
          $table->string('pickup_date');
          $table->string('drop_date');
          $table->integer('user_id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::drop('car_reservations');   //
    }
}
