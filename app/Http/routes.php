<?php

//Home routes

Route::get('/', 'WelcomeController@index');
Route::get('cars', 'WelcomeController@cars');
Route::get('about', 'WelcomeController@about');
Route::get('contact', 'WelcomeController@contact');
Route::get('dashboard', 'AuthController@dashboard');

Route::group(['middleware' => 'auth'], function(){
    Route::post('reservation', 'ReservationController@create');
    Route::get('reservation', 'ReservationController@car');
    Route::get('user/dashboard', 'ReservationController@dashboard');
    Route::post('reservation/pricing', 'ReservationController@pricing');
});

//Admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::get('dashboard', 'AdminController@index');

    //View routes
    Route::group(['prefix' => 'view'], function(){
        Route::get('{main}/{sub}', 'AdminController@view');
    });
    
    //Admin Action Routes
    Route::group(['prefix' => 'action'], function(){
        Route::post('location', 'LocationController@store');
        
        Route::post('pricing', 'TaxiPricingController@store');
        Route::delete('pricing', 'TaxiPricingController@destroy');
        Route::put('pricing', 'TaxiPricingController@update');

        Route::post('user', 'UserController@store');
        Route::delete('user', 'UserController@destroy');
        Route::put('user', 'UserController@update');
        
        Route::post('admin', 'AdminController@store');
        Route::delete('admin', 'AdminController@destroy');

        Route::post('car', 'CarController@store');
        Route::delete('car', 'CarController@destroy');
        Route::put('car', 'CarController@update');

        Route::post('car-type', 'CarTypeController@store');
        Route::delete('car-type', 'CarTypeController@destroy');
        Route::put('car-type', 'CarTypeController@update');

        Route::post('branch', 'BranchController@store');
        Route::delete('branch', 'BranchController@destroy');
        Route::put('branch', 'BranchController@update');

    });
});


//Authentication & User routes

Route::group(['prefix' => 'auth'], function(){
    Route::get('login', 'AuthController@login');
    Route::get('signup', 'AuthController@signup');

    Route::post('login', 'AuthController@authenticate');
    Route::post('register', 'UserController@store');

    Route::get('logout', 'AuthController@logout');
});