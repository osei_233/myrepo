<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;


class BranchController extends Controller
{
    public function store(Request $request)
    {
    	$branch = Branch::create($request->all());

    	if($branch->id){
    		return response()->json(['error' => false, 'message' => 'Branch added successfully!']);
    	}
    }


    public function destroy(Request $request)
    {
        $branch = Branch::find($request->input('id'));

        $branch->delete();
        $successfull = array('error' => false, 'message' => 'Branch deleted!');

        return response()->json($successfull, 200);
    }
}
