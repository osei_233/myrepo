<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Car;
use App\CarReservation;
use App\CarType;
use App\Branch;
use DB;
use Session;

class ReservationController extends Controller
{
    
    public function car(Request $request){
        if(!Session::has('state')) Session::put('state', 'reservation');

        $title = 'Reserve Car';
        $cars = Car::all();
        $branches = Branch::all();
        $car_types = CarType::all();
        $car_id = $request->input('car_id') ? $request->input('car_id'): '0';

        return view('reservation.car', compact('title', 'car_types', 'cars', 'branches', 'car_id'));
    }

    public function dashboard(){
        $car_reservations = CarReservation::where('user_id', Auth::user()->id)->get();
        
        return view('reservation.dashboard', compact('car_reservations'));
    }
    
    public function pricing(Request $request){
        $price = Car::find($request->input('car_id'))->pricing_per_day;
        
        return response()->json(array('error' => false, 'price' => $price), 200);
    }
    
    public function create(Request $request)
    {
        if($request->input('type') == 2){
            $reservation = TaxiReservation::create($request->all());
            $reservation->cost = self::getPricing($request->input('pickup_loc'), $request->input('drop_loc'));
        }
        else{
            $reservation = CarReservation::create($request->all());
        }

        $reservation->user_id = Auth::user()->id;
        $reservation->save();

        return view('reservation.success');
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
