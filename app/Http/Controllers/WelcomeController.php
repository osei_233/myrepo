<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Car;

class WelcomeController extends Controller
{
  
    public function index()
    {
        $cars = Car::limit(6)->get();
        $title = 'Home';

        return view('welcome', compact('cars', 'title'));
    }
    public function cars()
    {
        $cars = Car::all();

        // var_dump($cars);

        // return;
        $title = 'Car Listings';
        
        return view('car-listings', compact('cars', 'title'));
    }
    public function about(){
       return view('about');
    }
    
    public function contact(){
        
    }
}
