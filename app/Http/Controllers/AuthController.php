<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Gate;
use Session;
use App\Branch;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            
            return redirect()->intended();
        }
        else{
            return back()->withInput();
        }
    }
    
    public function logout(){
        Auth::logout();
        Session::forget('state');

        return redirect('/');
    }
    
    public function login(){
        $title = 'Login';
        return view('auth.login', compact('title'));
    }
    
    public function signup(){
        $title = 'Sign Up';
        $branches = Branch::all();
        return view('auth.signup', compact('title', 'branches'));
    }

    public function dashboard(){
w        if(Auth::user()->type == 2){
            return redirect('admin/dashboard');
        }else{
            return redirect('user/dashboard');
        }
    }
}
