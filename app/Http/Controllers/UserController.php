<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;


class UserController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        $user->password = Hash::make($request->input('password'));
        $user->branch_id = $request->input('branch');

        if($user->save()){
            if($request->ajax()){
                $successfull = array('error' => false, 'message' => 'User added!');
                return response()->json($successfull, 200);
            }

           return view('signup-success');
        }
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $user = User::find($id);
        $user->update($request->all());
        $successfull = array('error' => false, 'message' => 'User updated!');
        
        return response()->json($successfull, 200);
    }

    public function destroy(Request $request)
    {
        $user = User::find($request->get('id'));
        $user->delete();
        $successfull = array('error' => false, 'message' => 'User deleted!');

        return response()->json($successfull, 200);
    }
}