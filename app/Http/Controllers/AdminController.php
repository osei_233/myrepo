<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Car;
use App\CarType;
use App\Branch;
use App\Admin;
use App\CarReservation;
use App\Enquiry;
use Illuminate\Support\Facades\Hash;
use Gate;
use Auth;


class AdminController extends Controller
{
    private $modelList;

    function __construct(){
       $this->modelList = [
            'users/users'       => User::getData(),
            'users/admin'       => Admin::getData(),
            'cars/all'          => Car::getData(),
            'cars/types'        => CarType::all(),
            'branches/all'      => Branch::all(),
            'reservations/cars' => CarReservation::all(),
            'enquiries/all' => Enquiry::all()
        ];
    }

    public function index()
    {
            $admin = Admin::where('user_id', Auth::user()->id)->first();
            
            if($admin){
                $stat = array();
                $stat['cars'] = Car::all()->count();
                $stat['users'] = User::all()->count();
                $stat['admins'] = Admin::all()->count();
                $stat['branches'] = Branch::all()->count();
                $stat['enquiries'] = Enquiry::all()->count();
                $stat['reservations'] = CarReservation::all()->count();
                
                $username = Auth::user()->firstname.' '.Auth::user()->lastname;
                return view('admin.dashboard', compact('username', 'stat'));
            }
            else{
                return redirect('user/dashboard');
            }
    }

    public function view($main, $sub)
    {
        $route = $main.'/'.$sub;
        
        $viewTitle = ucfirst($main).' '.ucfirst($sub);
        $models = $this->modelList[$route];
        
        return view("admin.{$main}.{$sub}", compact('viewTitle', 'models'));
    }

    public function destroy(Request $request)
    {
        $admin = Admin::find($request->get('id'));
        $admin->delete();
        $successfull = array('error' => false, 'message' => 'Admin deleted!');

        return response()->json($successfull, 200);
    }

    public function store(Request $request)
    {
        $user = User::create($request->all());
        $user->password = Hash::make($request->input('password'));
        $user->branch_id = $request->input('branch');

        $user->save();

        $admin = new Admin();

        $admin->user_id = $user->id;
        $admin->branch_id = $user->branch_id;

        if($admin->save()){
            if($request->ajax()){
                $successfull = array('error' => false, 'message' => 'Admin added!');
                return response()->json($successfull, 200);
            }

           return view('signup-success');
        }
    }
}
