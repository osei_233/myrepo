<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CarType;

class CarTypeController extends Controller
{

    public function store(Request $request)
    {
        $car_type = CarType::create($request->all());
        $success = ($car_type instanceof CarType) ? true : false;
        
        $successfull = array('error' => false, 'message' => 'Car Type added!');
        $failed = array('error' => true, 'message' => 'Failed to add Car Type');
            
        return $success ? response()->json($successfull, 200) : response()->json($failed, 200);
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $type = CarType::find($id);

        $type->update($request->all());
        $successfull = array('error' => false, 'message' => 'Car Type updated!');
        
        return response()->json($successfull, 200);
    }

    public function destroy()
    {
        $id = Input::get('id');
        $car_type = CarType::find($id);
        $car_type->delete();
        $successfull = array('error' => false, 'message' => 'Car Type deleted!');

        return response()->json($successfull, 200);
    }
}
