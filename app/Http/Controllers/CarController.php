<?php namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Car;
use App\CarImage;

class CarController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $params = $request->all();
        $params['availability'] = $params['availability'] == 'on' ? 1 : 0;

        $car = Car::create($params);
        $success = ($car instanceof Car) ? true : false;

        // foreach ($request->file('image') as $key => $image) {
        //     // $file = $request->file('image');
        //     $file = $image;
        //     $filename = str_slug($request->input('name'), '_').time().'_'.$key.'.'.$file->getClientOriginalExtension();
        //     $file->move(public_path('img/cars'), $filename);

        //     $car_image = new CarImage;
        //     $imagePath = 'img/cars/'.$filename;

        //     $car_image->image_path = $imagePath;
        //     $car_image->car_id = $car->id;
            
        //     $car_image->save();
        // }
        
        $successfull = array('error' => false, 'message' => 'Car added!');
        $failed = array('error' => true, 'message' => 'Failed to add Car');
            
        return $success ? response()->json($successfull, 200) : response()->json($failed, 200);
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $car = Car::find($id);
        $car->update($request->all());
        $successfull = array('error' => false, 'message' => 'Car updated!');
        
        return response()->json($successfull, 200);
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');
        $car = Car::find($id);

        $car->delete();
        $successfull = array('error' => false, 'message' => 'Car deleted!');

        return response()->json($successfull, 200);
    }

    public function GetCars(){

        $cars = Car::all();
        foreach($cars as $car){
            $car->Owner;
            $car->CarType;
            $car->getGear();
            $car->features;
            $car->Image = $car->CarImage();
            $car->CarImages;

            foreach($car->features as $cft){
                $cft->feature;
            }
        }


        return response()->json(array(
                'error' => false,
                'message' => 'Successful',
                'cars' => $cars),
            200
        );
    }
}
