<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarReservation extends Model
{
	protected $guarded = ['_token', 'id'];

	public function car(){
		return $this->belongsTo('App\Car', 'car_id', 'id');
	}

	public function pick_up_branch(){
		return $this->belongsTo('App\Branch', 'pickup_branch', 'id');
	}

	public function drop_branch_name(){
		return $this->belongsTo('App\Branch', 'drop_branch', 'id');
	}

	public function user(){
		return $this->belongsTo('App\User', 'user_id', 'id');
	}
}
