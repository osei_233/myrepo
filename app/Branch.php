<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
	protected $guarded = ['id', 'updated_at', 'created_at'];
}
