<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Branch;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'firstname', 'email','telephone', 'lastname', 'sex'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function users(){
        return User::where('user_type', 1)->get();
    }

    public static function admins(){
        return;
    }

    public static function getData(){
        return [User::all(), Branch::all()];
    }
}
