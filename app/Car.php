<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
	protected $guarded = ['id', 'image[]'];
	
	public static function getData(){
		return ['cars' => Car::all(), 'types' => CarType::all(), 'branches' => Branch::all()];
	}

	public function images(){
		return $this->hasMany('App\CarImage', 'id', 'car_id');
	}
}
