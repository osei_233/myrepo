<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
	protected $guarded = ['id'];

	public static function getData(){
        return [Admin::all(), Branch::all()];
	}

	public function user(){
		return $this->hasOne('App\User', 'id', 'user_id');
	}

	public function branch(){
		return $this->hasOne('App\Branch', 'id', 'branch_id');
	}
}
